<?php 
$mode='product'; 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php require('struc_headtag.php'); ?>
<body>
<?php require('struc_head.php'); ?>
<?php require('struc_menu.php'); ?>	
		<div class="box4 last-box">
		<br>
		 		<p style="font-size:24px; font:Trebuchet MS;"><strong>Screws</strong></p>
				<h2></h2>
				<div class="entry">
						<div class="news">
						<ul>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/ExtrusionScrews.jpg" alt="#" /></a>								</div>
								<div class="post-data2">
									<p><strong>Extrution Screws</strong></p>
									<p>Extrusion screws enable you to maximize extrusion throughput, product quality and component life<a href="#">&hellip;</a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/NANOmix.jpg" alt="#" /></a>								</div>
								<div class="post-data2">
									<p><strong>Nano Mixer</strong></p>
									<p>Nano™ Mixer combines very intensive dispersionary mixing of colorants, fillers and additives with excellent temperature control<a href="#">&hellip;</a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/Stratablend2Mixeri.jpg" alt="#" /></a>								</div>
								<div class="post-data2">
									<p><strong>Stratablend® II Mixer</strong></p>
									<p>Stratablend® II Mixer can help you meet your quality and productivity goals<a href="#">&hellip;</a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/FusionDesignBarrierScrew.jpg" alt="#" /></a>								</div>
								<div class="post-data2">
									<p><strong>Fusion™ Design Barrier Screws</strong></p>
									<p>Fusion™ screw allows you to increase production and improve product quality in injection molding and extrusion process applications<a href="#">&hellip;</a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/InjectionScrews.jpg" alt="#" /></a>								</div>
								<div class="post-data2">
									<p><strong>Injection Screws</strong></p>
									<p>Injection screws can enable you to cut molding cycles and improve part quality with lower melt temperature, faster screw recovery and improved dispersion of color, additives and fillers <a href="#">&hellip;</a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/EasyMeltInjectionScrew.jpg" alt="#" /></a>								</div>
								<div class="post-data2">
									<p><strong>EasyMelt™ Injection Screws</strong></p>
									<p>EasyMelt™ combines the simplicity and economy of a conventional three-zone screw with the productivity and quality benefits of optimum material feeding, melting and metering<a href="#">&hellip;</a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/MeltProBarrierScrew.jpg" alt="#" /></a>								</div>
								<div class="post-data2">
									<p><strong>MeltPro™ Barrier Screw</strong></p>
									<p>MeltPro™ Barrier Screw is the building block of high performance barrier screw designs with tailorable properties to process crystalline and amorphous resins<a href="#">&hellip;</a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/DuPontELCeeScrew.jpg" alt="#" /></a>								</div>
								<div class="post-data2">
									<p><strong>DuPont® ELCee™ Screw</strong></p>
									<p> DuPont® ELCee™ Screw offers reduced screw recovery time, resulting in faster cycles and reduced quality problems related to melt over shearing<a href="#">&hellip;</a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/PulsarMixingScrew.jpg" alt="#" /></a>								</div>
								<div class="post-data2">
									<p><strong>Pulsar Mixing Screw</strong></p>
									<p>Pulsar® Mixing Screw offers a conventional style screw design with superior distributive mixing qualities. The turbulent reorientation of melt stream improves part uniformity.<a href="#">&hellip;</a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/Pulsar2MixingScrew.jpg" alt="#" /></a>								</div>
								<div class="post-data2">
									<p><strong>Pulsar® II Mixing Screw</strong></p>
									<p>Pulsar® II Mixing Screw offers a combination mixing screw design with good distributive and dispersive qualities<a href="#">&hellip;</a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/V-Mixer_fromCover.jpg" alt="#" /></a>								</div>
								<div class="post-data2">
									<p><strong>V-Mixer™ Screw</strong></p>
									<p>V-Mixer™ Screw offers a patented "pump-through" mixing design that generates localized high and low shear areas with low mixer pressure drop<a href="#">&hellip;</a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/Z-MixerScrew.jpg" alt="#" /></a>								</div>
								<div class="post-data2">
									<p><strong>Z-Mixer™ Screw</strong></p>
									<p>Z-Mixer™ Screw is the best dispersive mixing screw in the industry<a href="#">&hellip;</a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/FusionII-big.jpg" alt="#" /></a>								</div>
								<div class="post-data2">
									<p><strong>Fusion™ II Screws</strong></p>
									<p>Fusion™ II screw delivers enhanced chaotic mixing while retaining the productivity benefits of faster plastication and lower melt temperature<a href="#">&hellip;</a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="cl">&nbsp;</div>
		</div>
		<!-- /box4 -->
		</div>
	<!-- /shell-->		
	<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
</div>
<!-- /main -->
<?php require('struc_footer.php'); ?>	
</body>
</html>