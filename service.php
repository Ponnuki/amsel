<?php 
$mode='service'; 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php require('struc_headtag.php'); ?>
<?php require('struc_colorbox.php'); ?>
<body>
<?php require('struc_head.php'); ?>
<br>
<!-- /Header -->
<div id="main">
	<div class="shell">
		<!-- Box -->
		<div class="box2">
			<h2>Training</h2>
			
		  <div class="entry">
				<div class="big-image"><img src="css/images/welcome.jpg" alt="#" /></div>	
				<p><strong>Training : We Can Help You Work Smarter </strong></p>
			  <p>Profit from our experience and know - how in plastic processing and equipment by calling on us to train your key people &hellip;</p>
		  </div>
		</div>
		<!-- /Box -->
		<!-- Box -->
		<div class="box2">
			<h2>Support</h2>
			
			<div class="entry">
				<!-- News -->	
				<div class="news">
					<ul>
						<li>
							<div class="post-image">
								<a href="#"><img src="css/images/news-image-1.jpg" alt="" /></a>
							</div>
							<div class="post-data">
								<p><strong>Training</strong></p>
								<p> Profit from our experience and know-how in plastics processing and equipment by having your staff trained by our professionals<a href="#">&hellip;</a></p>
							</div>
							<div class="cl">&nbsp;</div>
						</li>
						<li>
						
							<div class="post-image">
								<a href="#"><img src="css/images/news-image-2.jpg" alt="" /></a>
							</div>
							<div class="post-data">
								<p><strong>Process Development</strong></p>
								<p>You need help developing new processes? We will be pleased to assist you by using our labs and know-how. Please <a href="contact.php">contact us</a> for additional information.</p>
							</div>
							<div class="cl">&nbsp;</div>
						</li>
						<li>
							<div class="post-image">
								<a href="#"><img src="css/images/news-image-3.jpg" alt="" /></a>
							</div>
							<div class="post-data">
								<p><strong>Installation & Startup Help</strong></p>
								<p> You need installation &amp; startup help? Please <a href="contact.php">contact us</a> for additional information. </p>
							</div>
							<div class="cl">&nbsp;</div>
						</li>
					</ul>
				</div>
				<!-- /News -->
			</div>
			
			
		</div>
		<!-- /Box -->
		<!-- Box -->
	  <div class="box2 last-box">
			<h2>Return & Repair</h2>
			
		  <p><strong>Quality awareness, quick delivery, competitive pricing and hassle-free customer service</strong></p>
	      <p>Our computerized scheduling system ensures accurate delivery quotes at the time of order.</p>
		  <p>Customer service programs to many high volume users. We welcome the opportunity to discuss your requirements.</p>
	    </div>
		<!-- /Box -->
		<div class="cl">&nbsp;</div>
	</div>
	<br /><br /><br /><br /><br /><br /><br /><br /><br />
</div>
<!-- /Main -->
<!-- Footer -->
<?php require('struc_footer.php'); ?>
<!-- /Footer -->
</body>
</html>
