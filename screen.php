<?php 
$mode='product'; 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php require('struc_headtag.php'); ?>
<?php require('struc_colorbox.php'); ?>
<body>
<?php require('struc_head.php'); ?>
<?php require('struc_menu.php'); ?>	
		<!-- /box3 -->
		<!-- box4-->		
		<div class="box4 last-box">
		<br>
		 		<p style="font-size:24px; font:Trebuchet MS;"><strong>Screen Changer</strong></p>
				<h2></h2>
				<div class="entry">
						<div class="news">
						<ul>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/ManualScreenChangers_EM-XM.jpg" alt="#" /></a>
								</div>
								<div class="post-data2">
									<p><strong>Manual Screen Changers</strong></p>
									<p>From the worldwide leader in filtration equipment, Xaloy offers the EM and XM manual screen changers<a class="group1" href="content/manual.jpg" title="Manual Screen Changers">&hellip;</a><a class="group1" href="content/manual(2).jpg" title="Manual Screen Changers"></a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/HydraulicScreenChanger.jpg" alt="#" /></a>
								</div>
								<div class="post-data2">
									<p><strong>Hydraulic Screen Changers</strong></p>
									<p>From the worldwide leader in filtration equipment, Xaloy offers the EH hydraulic screen changers<a class="group2" href="content/hydra.jpg" title="Hydraulic Screen Changers">&hellip;</a><a class="group2" href="content/hydra(2).jpg" title="Hydraulic Screen Changers"></a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/DBCContinousScreenChanger1.jpg" alt="#" /></a>
								</div>
								<div class="post-data2">
									<p><strong>DBC Continuos Screen Changers</strong></p>
									<p>DBC Continuous Screen Changer that provides continuous polymer flow and consistent, repeatable process parameters during screen changes<a class="group3" href="content/dcb.jpg" title="DBC Continuos Screen Changers">&hellip;</a><a class="group3" href="content/dcb(2).jpg" title="DBC Continuos Screen Changers"></a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/HighCapacityScreenChanger(2).jpg" alt="#" /></a>
								</div>
								<div class="post-data2">
									<p><strong>High Capacity Screen Changers</strong></p>
									<p>Xaloy's line of High Capacity Screen Changers is the result of 55 years of experience, innovation and development in filtration systems<a class="group4" href="content/high.jpg" title="High Capacity Screen Changers">&hellip;</a><a class="group4" href="content/high(2).jpg" title="High Capacity Screen Changers"></a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="cl">&nbsp;</div>
					
		</div>
		<!-- /box4 -->
		</div>
	<!-- /shell-->		
	<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
</div>
<!-- /main -->
<?php require('struc_footer.php'); ?>
</body>
</html>