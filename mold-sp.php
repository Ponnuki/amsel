<?php 
$mode='product'; 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php require('struc_headtag.php'); ?>
<?php require('struc_colorbox.php'); ?>
<body>
<?php require('struc_head.php'); ?>
<?php require('struc_menu.php'); ?>	
		<!-- /box3 -->
		<!-- box4-->		
		<div class="box4 last-box">
		<br>
		 		<p style="font-size:24px; font:Trebuchet MS;"><strong>Mold Release Spray</strong></p>
				<h2></h2>
				<div class="entry">
						<div class="news">
						<ul>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/spray/S3 5.jpg" alt="#" /></a>
								</div>
								<div class="post-data2">
									<p><strong>Pelicoat Paintable</strong></p>
									<p>Pelicoat Paintable (S3-5) has secondary manufacturing properties, when painting, printing, glinding or hot stamping is further applied on injection part<a class="group1" href="content/s35.jpg" title="Pelicoat Paintable">&hellip;</a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/spray/ALPHA.jpg" alt="#" /></a>
								</div>
								<div class="post-data2">
									<p><strong>Pelicoat Alpha</strong></p>
									<p>Pelicoat Alpha has outstanding release performance. It is only applicable to injection parts without secondary manufacturing process such as painting, printing, glinding or hot stamping<a class="group2" href="content/alpha.jpg" title="Pelicoat Alpha">&hellip;</a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/spray/RF.jpg" alt="#" /></a>
								</div>
								<div class="post-data2">
									<p><strong>Pelicoat RF</strong></p>
									<p>Pelicoat RF has good performance for reducing defective ratio of the injection parts and improving accuracy of dimensions<a class="group3" href="content/rf.jpg" title="Pelicoat RF">&hellip;</a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							
						</ul>
					</div>
				</div>
				<div class="cl">&nbsp;</div>
					
		</div>
		<!-- /box4 -->
		</div>
	<!-- /shell-->		
	<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
</div>
<!-- /main -->
<?php require('struc_footer.php'); ?>
</body>
</html>