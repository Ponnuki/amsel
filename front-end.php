<?php 
$mode='product'; 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php require('struc_headtag.php'); ?>
<?php require('struc_colorbox.php'); ?>
<body>
<?php require('struc_head.php'); ?>
<?php require('struc_menu.php'); ?>	
		<!-- /box3 -->
		<!-- box4-->		
		<div class="box4 last-box">
		<br>
		 		<p style="font-size:24px; font:Trebuchet MS;"><strong>Front End Components</strong></p>
				<h2></h2>
				<div class="entry">
						<div class="news">
						<ul>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/Nozzle_Endcaps_grp_HRretOnK.jpg" alt="#" /></a>
								</div>
								<div class="post-data2">
									<p><strong>Front End Components</strong></p>
									<p>Design and manufacture a complete line of front end components for injection units from a variety of abrasion and corrosion resistant steels<a class="group3" href="content/front.png" title="Front End Components">&hellip;</a><a class="group3" href="content/front(2).png" title="Front End Components"></a><a class="group3" href="content/front(3).png" title="Front End Components"></a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/AutoShutValve.jpg" alt="#" /></a>
								</div>
								<div class="post-data2">
									<p><strong>Auto-Shut Valves</strong></p>
									<p>Comprehensive line of front end components are made to OEM original specifications with improved wear and corrosion resistance<a class="group2" href="content/front.png" title="Front End Components">&hellip;</a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/FTR_Valves.jpg" alt="#" /></a>
								</div>
								<div class="post-data2">
									<p><strong>FTR Valves</strong></p>
									<p>Comprehensive line of front end components are made to OEM original specifications with improved wear and corrosion resistance.<a class="group1" href="content/front.png" title="Front End Components">&hellip;</a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/SlidingRingValve.jpg" alt="#" /></a>
								</div>
								<div class="post-data2">
									<p><strong>Sliding Ring Valves</strong></p>
									<p>Xaloy's comprehensive line of front end components are made to OEM original specifications with improved wear and corrosion resistance<a class="group2" href="content/front(2).png" title="Front End Components">&hellip;</a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/LockingRingValve.jpg" alt="#" /></a>
								</div>
								<div class="post-data2">
									<p><strong>Locking Ring Valves</strong></p>
									<p>Comprehensive line of front end components are made to OEM original specifications with improved wear and corrosion resistance<a class="group1" href="content/front(2).png" title="Front End Components">&hellip;</a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/Ball-Check-Valve.jpg" alt="#" /></a>
								</div>
								<div class="post-data2">
									<p><strong>Poly-Check Valves</strong></p>
									<p>Comprehensive line of front end components are made to OEM original specifications with improved wear and corrosion resistance<a class="group2" href="content/front(3).png" title="Front End Components">&hellip;</a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/EliminatorNozzle.jpg" alt="#" /></a>
								</div>
								<div class="post-data2">
									<p><strong>Eliminator Nozzle Tips</strong></p>
									<p>Comprehensive line of front end components are made to OEM original specifications with improved wear and corrosion resistance.<a class="group1" href="content/front(3).png" title="Front End Components">&hellip;</a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="cl">&nbsp;</div>
					
		</div>
		<!-- /box4 -->
		</div>
		<p>&nbsp;</p>
	<!-- /shell-->		
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br></div>
<!-- /main -->
<?php require('struc_footer.php'); ?>
</body>
</html>