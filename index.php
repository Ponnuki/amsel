<?php 
$mode='home'; 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php require('struc_headtag.php'); ?>
<body>
<?php require('struc_head.php'); ?>

<!-- /Header -->

<!-- Intro -->
<div id="intro">
	<div class="shell">
		<!-- Slider Holder -->
		<div class="slider-holder">
			<ul>
				<!-- Offer -->
				<li>
					<div class="offer-image">
						<img src="css/images/offer-image-2.jpg" alt="" />
					</div>
					<div class="offer-data">
						<h3>Extrusion Barrels</h3>
						
						<div class="entry">
						<p></p>
							<p>X-800 and X-102 barrels meet the needs for wear and corrosion resistance for more than 90% of all extrusion requirements.</p>
						</div>
						
						<div class="buttons">
							<a href="product.php" class="button"><span>Read More</span></a>
						</div>
					</div>
				</li>
				<!-- /Offer -->
				
				<!-- Offer -->
				<li>
					<div class="offer-image">
						<img src="css/images/offer-image-1.jpg" alt="" />
					</div>
					<div class="offer-data">
						<h3>EasyMelt Injection Screws</h3>
						
						<div class="entry">
							<p></p>
							<p>EasyMelt combines the simplicity and economy of a conventional three-zone screw with the productivity and quality benefits of optimum material feeding, melting and metering.</p>
						</div>
						
						<div class="buttons">
							<a href="product.php" class="button"><span>Read More</span></a>
						</div>
					</div>
				</li>
				<!-- /Offer -->
				
				<!-- Offer -->
			  <li>
					<div class="offer-image">
						<img src="css/images/offer-image-3.jpg" alt="" />
					</div>
					<div class="offer-data">
						<h3>Standard Melt Pumps</h3>
						
						<div class="entry">
							<p></p>
							<p>Standard Melt Pumps installation provides you with more production line flexibility.</p>
						</div>
						
						<div class="buttons">
							<a href="product.php" class="button"><span>Read More</span></a>
						</div>
					</div>
			  </li>
				<!-- /Offer -->
				
				<!-- Offer -->
				<li>
					<div class="offer-image">
						<img src="css/images/offer-image-4.jpg" alt="" />
					</div>
					<div class="offer-data">
						<h3>Hydraulic Screen Changers</h3>
						
						<div class="entry">
							<p></p>
							<p>From the worldwide leader in filtration equipment, Xaloy offers the EH hydraulic screen changers.</p>
						</div>
						
						<div class="buttons">
							<a href="product.php" class="button"><span>Read More</span></a>
						</div>
					</div>
				</li>
				<!-- /Offer -->
				
				<!-- Offer -->
				<li>
					<div class="offer-image">
						<img src="css/images/offer-image-5.jpg" alt="" />
					</div>
					<div class="offer-data">
						<h3>Locking Ring Valves</h3>
						
						<div class="entry">
							<p></p>
							<p>Comprehensive line of front end components are made to OEM original specifications with improved wear and corrosion resistance.</p>
						</div>
						
						<div class="buttons">
							<a href="product.php" class="button"><span>Read More</span></a>
						</div>
					</div>
				</li>
				<!-- /Offer -->
			</ul>
		</div>
		<!-- /Slider Holder -->
		<!-- Slider Navigation -->
		<div class="slider-navigation">
			<ul>
				<li><a href="#">1</a></li>
				<li><a href="#">2</a></li>
				<li><a href="#">3</a></li>
				<li><a href="#">4</a></li>
				<li><a href="#">5</a></li>
			</ul>
		</div>
		<!-- /Slider Navigation -->
	</div>
</div>
<!-- /Intro -->
<br /><br /><br /><br />
<!-- Main -->

<!-- /Main -->

<!-- Footer -->
<?php require('struc_footer.php'); ?>
<!-- /Footer -->
</body>
</html>