<?php 
$mode='product'; 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php require('struc_headtag.php'); ?>
<?php require('struc_colorbox.php'); ?>
<body>
<?php require('struc_head.php'); ?>
<?php require('struc_menu.php'); ?>	
		<!-- /box3 -->
		<!-- box4-->		
		<div class="box4 last-box">
		<br>
		 		<p style="font-size:24px; font:Trebuchet MS;"><strong>Screws for Injection</strong></p>
				<h2></h2>
				<div class="entry">
						<div class="news">
						<ul>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/InjectionScrews.jpg" alt="#" /></a>
								</div>
								<div class="post-data2">
									<p><strong>Injection Screws</strong></p>
									<p>Injection screws can enable you to cut molding cycles and improve part quality with lower melt temperature, faster screw recovery and improved dispersion of color, additives and fillers<a class="group1" href="content/in-screws.jpg" title="Extrution Screws">&hellip;</a><a class="group1" href="content/in-screws(2).jpg" title="Extrution Screws"></a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/EasyMeltInjectionScrew.jpg" alt="#" /></a>
								</div>
								<div class="post-data2">
									<p><strong>EasyMelt™ Injection Screws</strong></p>
									<p>EasyMelt™ combines the simplicity and economy of a conventional three-zone screw with the productivity and quality benefits of optimum material feeding, melting and metering<a class="group2" href="content/easy.jpg" title="EasyMelt™ Injection Screws">&hellip;</a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/MeltProBarrierScrew.jpg" alt="#" /></a>
								</div>
								<div class="post-data2">
									<p><strong>MeltPro™ Barrier Screw</strong></p>
									<p>MeltPro™ Barrier Screw is the building block of high performance barrier screw designs with tailorable properties to process crystalline and amorphous resins<a class="group3" href="content/meltpro.jpg" title="MeltPro™ Barrier Screw">&hellip;</a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/DuPontELCeeScrew.jpg" alt="#" /></a>
								</div>
								<div class="post-data2">
									<p><strong>DuPont® ELCee™ Screw</strong></p>
									<p> DuPont® ELCee™ Screw offers reduced screw recovery time, resulting in faster cycles and reduced quality problems related to melt over shearing<a class="group3" href="content/dupont.jpg" title="DuPont® ELCee™ Screw">&hellip;</a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/PulsarMixingScrew.jpg" alt="#" /></a>
								</div>
								<div class="post-data2">
									<p><strong>Pulsar Mixing Screw</strong></p>
									<p>Pulsar® Mixing Screw offers a conventional style screw design with superior distributive mixing qualities. The turbulent reorientation of melt stream improves part uniformity.<a class="group4" href="content/pulsar.jpg" title="Pulsar Mixing Screw">&hellip;</a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/Pulsar2MixingScrew.jpg" alt="#" /></a>
								</div>
								<div class="post-data2">
									<p><strong>Pulsar® II Mixing Screw</strong></p>
									<p>Pulsar® II Mixing Screw offers a combination mixing screw design with good distributive and dispersive qualities<a class="group5" href="content/pulsar2.jpg" title="Pulsar® II Mixing Screw">&hellip;</a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/V-Mixer_fromCover.jpg" alt="#" /></a>
								</div>
								<div class="post-data2">
									<p><strong>V-Mixer™ Screw</strong></p>
									<p>V-Mixer™ Screw offers a patented "pump-through" mixing design that generates localized high and low shear areas with low mixer pressure drop<a class="group6" href="content/v.jpg" title="V-Mixer™ Screw">&hellip;</a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/Z-MixerScrew.jpg" alt="#" /></a>
								</div>
								<div class="post-data2">
									<p><strong>Z-Mixer™ Screw</strong></p>
									<p>Z-Mixer™ Screw is the best dispersive mixing screw in the industry<a class="group7" href="content/z.jpg" title="Z-Mixer™ Screw">&hellip;</a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/FusionII-big.jpg" alt="#" /></a>
								</div>
								<div class="post-data2">
									<p><strong>Fusion™ II Screws</strong></p>
									<p>Fusion™ II screw delivers enhanced chaotic mixing while retaining the productivity benefits of faster plastication and lower melt temperature<a class="group8" href="content/fusion2.jpg" title="Fusion™ II Screws">&hellip;</a><a class="group8" href="content/fusion2(2).jpg" title="Fusion™ II Screws"></a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="cl">&nbsp;</div>
					
		</div>
		<!-- /box4 -->
		</div>
	<!-- /shell-->		
	<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
</div>
<!-- /main -->
<?php require('struc_footer.php'); ?>
</body>
</html>