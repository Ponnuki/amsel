<?php 
$mode='product'; 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php require('struc_headtag.php'); ?>
<?php require('struc_colorbox.php'); ?>
<body>
<?php require('struc_head.php'); ?>
<?php require('struc_menu.php'); ?>	
		<!-- /box3 -->
		<!-- box4-->		
		<div class="box4 last-box">
		<br>
		 		<p style="font-size:24px; font:Trebuchet MS;"><strong>Cleaning Spray</strong></p>
				<h2></h2>
				<div class="entry">
						<div class="news">
						<ul>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/spray/GP C.jpg" alt="#" /></a>
								</div>
								<div class="post-data2">
									<p><strong>GP Cleaner(Multipurpose Cleaner)</strong></p>
									<p>To remove grease and oil from surface of molds, products, macgineries, equipments and so on. It is recommended to use together with "Depo Cleaner" for mold cleaning<a class="group1" href="content/gp.jpg" title="GP Cleaner(Multipurpose Cleaner)">&hellip;</a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/spray/gp c.jpg" alt="#" /></a>
								</div>
								<div class="post-data2">
									<p><strong>B & P Cleaner C5, Jumbo(Intruments Cleaner)</strong></p>
									<p>B & P Cleaner is designed to completely flush out resin powder or metal powder from hydraulic eqiupments such as brake drum, lining, brake pad, chain, gearbox and bearing<a class="group2" href="content/bp.jpg" title="B & P Cleaner C5, Jumbo(Intruments Cleaner)">&hellip;</a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/spray/depo.jpg" alt="#" /></a>
								</div>
								<div class="post-data2">
									<p><strong>Depo Cleaner(Deposit Remover)</strong></p>
									<p>Depo cleaner is designed to remove and dissolve the sticky resin stain and deposit, caused by gas in molding,out from molds and products. After sprayed on the mold, leave it for 2-3 minutes until the agent penetrate into the stain then wipe out by clothe or "GP Cleaner"<a class="group3" href="content/depo.jpg" title="Depo Cleaner(Deposit Remover)">&hellip;</a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="cl">&nbsp;</div>
					
		</div>
		<!-- /box4 -->
		</div>
	<!-- /shell-->		
	<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
</div>
<!-- /main -->
<?php require('struc_footer.php'); ?>
</body>
</html>