<?php 
$mode='product'; 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php require('struc_headtag.php'); ?>
<?php require('struc_colorbox.php'); ?>
<body>
<?php require('struc_head.php'); ?>
<?php require('struc_menu.php'); ?>	
		<!-- /box3 -->
		<!-- box4-->		
		<div class="box4 last-box">
		<br>
		 		<p style="font-size:24px; font:Trebuchet MS;"><strong>Pelletizer</strong></p>
				<h2></h2>
				<div class="entry">
						<div class="news">
						<ul>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/WRP-12i-In-Line-Pelletizer.jpg" alt="#" /></a>
								</div>
								<div class="post-data2">
									<p><strong>WRP-12i In Line Pelletizer</strong></p>
									<p>Xaloy's water ring pelletizer connects directly to the end of the extruder, Screen Changer or Melt Pump<a class="group1" href="content/12.jpg" title="WRP-12i In Line Pelletizer">&hellip;</a><a class="group1" href="content/12(2).jpg" title="WRP-12i In Line Pelletizer"></a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/WRP12i_Inline-Pellitizer.jpg" alt="#" /></a>
								</div>
								<div class="post-data2">
									<p><strong>WRP-35s/36s Pelletizer</strong></p>
									<p>Xaloy's Water Ring Pelletizing Systems offer a modern, cost-effective method of pelletizing a wide range of unfilled and filled thermoplastic polymers<a class="group2" href="content/35.jpg" title="WRP-35s/36s Pelletizer">&hellip;</a><a class="group2" href="content/35(2).jpg" title="WRP-35s/36s Pelletizer"></a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="cl">&nbsp;</div>
					
		</div>
		<!-- /box4 -->
		</div> 
		<p>&nbsp;</p>
		
	<!-- /shell-->		
	<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

</div>
<!-- /main -->
<?php require('struc_footer.php'); ?>
</body>
</html>