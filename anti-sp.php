<?php 
$mode='product'; 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php require('struc_headtag.php'); ?>
<?php require('struc_colorbox.php'); ?>
<body>
<?php require('struc_head.php'); ?>
<?php require('struc_menu.php'); ?>	
		<!-- /box3 -->
		<!-- box4-->		
		<div class="box4 last-box">
		<br>
		 		<p style="font-size:24px; font:Trebuchet MS;"><strong>Anti Rust Spray</strong></p>
				<h2></h2>
				<div class="entry">
						<div class="news">
						<ul>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/spray/EF C.jpg" alt="#" /></a>
								</div>
								<div class="post-data2">
									<p><strong>Efflux F/Colored(solid film rust preventive for molds)</strong></p>
									<p>The oilless anti-rust spray uses to protect mold from rust and dust, especially for the mold which dislike oil<a class="group1" href="content/ef.jpg" title="Efflux F/Colored(solid film rust preventive for molds)">&hellip;</a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/spray/SABI.jpg" alt="#" /></a>
								</div>
								<div class="post-data2">
									<p><strong>Sabicro(Semi-rigid film rust preventive for molds)</strong></p>
									<p>The oil anti-rust spray has excellent rust preventive performance among oil spray type<a class="group2" href="content/sabi.jpg" title="Sabicro(Semi-rigid film rust preventive for molds)">&hellip;</a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/spray/12.jpg" alt="#" /></a>
								</div>
								<div class="post-data2">
									<p><strong>Pelicoat 12(Multipurpose rust preventive & lubricant)</strong></p>
									<p>Multipurpose spray use for rust preventive and lubrication of machineries and instrument<a class="group3" href="content/poli12.jpg" title="Pelicoat 12(Multipurpose rust preventive & lubricant)">&hellip;</a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="cl">&nbsp;</div>
					
		</div>
		<!-- /box4 -->
		</div>
	<!-- /shell-->		
	<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
</div>
<!-- /main -->
<?php require('struc_footer.php'); ?>	

</body>
</html>