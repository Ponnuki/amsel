<?php 
$mode='product'; 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php require('struc_headtag.php'); ?>
<?php require('struc_colorbox.php'); ?>
<body>
<?php require('struc_head.php'); ?>
<?php require('struc_menu.php'); ?>	
		<!-- /box3 -->
		<!-- box4-->		
		<div class="box4 last-box">
		<br>
		 		<p style="font-size:24px; font:Trebuchet MS;"><strong>Screws for Extrusion</strong></p>
				<h2></h2>
				<div class="entry">
						<div class="news">
						<ul>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/ExtrusionScrews.jpg" alt="#" /></a>
								</div>
								<div class="post-data2">
									<p><strong>Extrution Screws</strong></p>
									<p>Extrusion screws enable you to maximize extrusion throughput, product quality and component life<a class="group1" href="content/ex-screws.jpg" title="Extrution Screws">&hellip;</a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/NANOmix.jpg" alt="#" /></a>
								</div>
								<div class="post-data2">
									<p><strong>Nano Mixer</strong></p>
									<p>Nano™ Mixer combines very intensive dispersionary mixing of colorants, fillers and additives with excellent temperature control<a class="group2" href="content/nano-mix.jpg" title="Nano Mixer">&hellip;</a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/Stratablend2Mixeri.jpg" alt="#" /></a>
								</div>
								<div class="post-data2">
									<p><strong>Stratablend® II Mixer</strong></p>
									<p>Stratablend® II Mixer can help you meet your quality and productivity goals<a class="group3" href="content/strata-mixer.jpg" title="Stratablend® II Mixer">&hellip;</a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/FusionDesignBarrierScrew.jpg" alt="#" /></a>
								</div>
								<div class="post-data2">
									<p><strong>Fusion™ Design Barrier Screws</strong></p>
									<p>Fusion™ screw allows you to increase production and improve product quality in injection molding and extrusion process applications<a class="group4" href="content/fusion.jpg" title="Fusion™ Design Barrier Screws">&hellip;</a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="cl">&nbsp;</div>
					
		</div>
		<!-- /box4 -->
		</div>
	<!-- /shell-->		
	<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
</div>
<!-- /main -->
<?php require('struc_footer.php'); ?>
</body>
</html>