<br>
<!-- main -->
<div id="main">
	<!-- shell -->
	<div class="shell">
		<!-- box3-->
		<div class="box3">
			<!-- menu -->
			<div class="menu">
				<ul class="top-level">
					
					<li><a>Screws</a>
						<ul class="sub-level">
							<li><a href="pro-scr-ex.php">Extrusion</a></li>
							<li><a href="pro-scr-in.php">Injection</a></li>
						</ul>
					</li>
					<li><a>Barrels</a>
						<ul class="sub-level">
							<li><a href="pro-bar-ex.php">Extrusion</a></li>
							<li><a href="pro-bar-in.php">Injection</a></li>
						</ul>
					</li>
		
					<li><a href="front-end.php">Front End Component</a></li>
					<li><a href="mixer.php">Mixer</a></li>
					<li><a href="roll.php">Rolls</a></li>
					<li><a href="gear.php">Gear Pump</a></li>
					<li><a href="pelletizer.php">Pelletizer</a></li>
					<li><a href="screen.php">Screen Changer</a></li>
					<li><a href="cleaner.php">Set Cleaner</a></li>
					
					
					<li><a>Spray</a>
						<ul class="sub-level">
							<li><a href="mold-sp.php">Mold Release Spray</a></li>
							<li><a href="lub-sp.php">Lubricant Spray</a></li>
							<li><a href="anti-sp.php">Anti Rust Spray</a></li>
							<li><a href="cle-sp.php">Cleaning Spray</a></li>
						</ul>
					</li>
				</ul>
			<!-- /menu-->
			</div>	
		<!-- box3-->
		</div>