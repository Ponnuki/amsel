<?php
$mode='contact';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php require('struc_headtag.php'); ?>
<body>
<?php require('struc_head.php'); ?>
<!-- /Header -->
<!-- Main --><br>
<div id="main">
  <div class="shell">

		<div class="box">
			<h2>Location</h2>

			<div class="entry">
				<div class="big-image"><iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.co.th/maps?f=d&amp;source=s_d&amp;saddr=%E0%B9%80%E0%B8%AA%E0%B8%A3%E0%B8%B5+7&amp;daddr=&amp;geocode=FVSv0QAd5HP_BQ&amp;sll=13.74186,100.627465&amp;sspn=0.002955,0.005284&amp;t=m&amp;hl=th&amp;mra=ls&amp;ie=UTF8&amp;ll=13.74186,100.627465&amp;spn=0.002955,0.005284&amp;output=embed"></iframe><br /><small><a href="https://maps.google.co.th/maps?f=d&amp;source=embed&amp;saddr=%E0%B9%80%E0%B8%AA%E0%B8%A3%E0%B8%B5+7&amp;daddr=&amp;geocode=FVSv0QAd5HP_BQ&amp;sll=13.74186,100.627465&amp;sspn=0.002955,0.005284&amp;t=m&amp;hl=th&amp;mra=ls&amp;ie=UTF8&amp;ll=13.74186,100.627465&amp;spn=0.002955,0.005284" style="color:#0000FF;text-align:left">&#3604;&#3641;&#3649;&#3612;&#3609;&#3607;&#3637;&#3656;&#3586;&#3609;&#3634;&#3604;&#3651;&#3627;&#3597;&#3656;&#3586;&#3638;&#3657;&#3609;</a></small></div>
				<p><strong>2/25 Seree 4 Road, Suanluang, Bangkok 10250 </strong></p>
				<p><strong>Tel</strong>: 02-7201411-2, 02-7183611-2 </p>
			  	<p><strong>Fax</strong>: 02-7183979</p>
			</div>


		</div>
		<div class="last-box">
			<h2>Contact us</h2>

			<div class="entry">
				<!-- News -->
			  <p><strong>Marketing Service</strong></p>
			  <p><strong>Tel</strong>: 081-3123278</p>
			  <p><strong>Email</strong>: wasin.t@amsel.co.th</p>
			  <p><strong>Customer Service</strong></p>
			  <p><strong>Tel</strong>: 085-9736958</p>
			  <p><strong>Email</strong>: suphattra.i@amsel.co.th</p>

			</div>
			<div class="cl">&nbsp;</div>

		</div>

	</div>
	<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
</div>
<?php require('struc_footer.php'); ?>
</body>
</html>
