<?php 
$mode='product'; 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php require('struc_headtag.php'); ?>
<?php require('struc_colorbox.php'); ?>
<body>
<?php require('struc_head.php'); ?>
<?php require('struc_menu.php'); ?>	
		<!-- /box3 -->
		<!-- box4-->		
		<div class="box4 last-box">
		<br>
		 		<p style="font-size:24px; font:Trebuchet MS;"><strong>Set Cleaner</strong></p>
				<h2></h2>
				<div class="entry">
						<div class="news">
						<ul>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/MiniJetCleaner.jpg" alt="#" /></a>
								</div>
								<div class="post-data2">
									<p><strong>Mini Jet Cleaner™</strong></p>
									<p>The Mini jet is the smallest unit in the Xaloy Jet Cleaner Product Line. It utilizes heat and vacuum to effect vaporization<a class="group1" href="content/minijet.jpg" title="Mini Jet Cleaner">&hellip;</a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/JC_1724_0036-NEW.jpg" alt="#" /></a>
								</div>
								<div class="post-data2">
									<p><strong>JCP 1724 Jet Cleaner™</strong></p>
									<p>The Model JCP 1724 Jet Cleaner™ represents one of the most popular model sizes of the Xaloy Jet Cleaner product line<a class="group2" href="content/jet1724.jpg" title="Jet 1724 Jet Cleaner">&hellip;<a class="group2" href="content/jet1724(2).jpg" title="Jet 1724 Jet Cleaner"></a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="cl">&nbsp;</div>
					
		</div>
		<!-- /box4 -->
		</div>
		<p>&nbsp;</p>
	<!-- /shell-->		
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

</div>
<!-- /main -->
<?php require('struc_footer.php'); ?>
</body>
</html>