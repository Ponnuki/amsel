<?php 
$mode='product'; 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php require('struc_headtag.php'); ?>
<?php require('struc_colorbox.php'); ?>
<body>
<?php require('struc_head.php'); ?>
<?php require('struc_menu.php'); ?>	
		<!-- /box3 -->
		<!-- box4-->		
		<div class="box4 last-box">
		<br>
		 		<p style="font-size:24px; font:Trebuchet MS;"><strong>Gear Pump</strong></p>
				<h2></h2>
				<div class="entry">
						<div class="news">
						<ul>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/StandardMeltPumps.jpg" alt="#" /></a>
								</div>
								<div class="post-data2">
									<p><strong>Standard Melt Pumps</strong></p>
									<p>Standard Melt Pumps installation provides you with more production line flexibility<a class="group1" href="content/standard.jpg" title="Standard Melt Pumps">&hellip;</a><a class="group1" href="content/standard(2).jpg" title="Standard Melt Pumps"></a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/HighPressurePump.jpg" alt="#" /></a>
								</div>
								<div class="post-data2">
									<p><strong>High Pressure Melt Pumps</strong></p>
									<p>High Pressure Melt Pumps is a positive displacement device which provides a linear output over a wide range of operating conditions<a class="group2" href="content/high-press(2).jpg" title="High Pressure Melt Pumps">&hellip;</a><a class="group2" href="content/high-press.jpg" title="High Pressure Melt Pumps"></a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="cl">&nbsp;</div>
					
		</div>
		<!-- /box4 -->
		</div> 
		<p>&nbsp;</p>
		
	<!-- /shell-->		
	<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

</div>
<!-- /main -->
<?php require('struc_footer.php'); ?>
</body>
</html>