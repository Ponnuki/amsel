<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<title>Amsel</title>
	<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
	
	<!--[if IE 6]>
		<link rel="stylesheet" href="css/ie.css" type="text/css" media="all" />	
	<![endif]-->
	
	<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
	
	<script type="text/javascript" src="js/jquery.jcarousel.js"></script>
	
	<!-- Cufon -->
	<script type="text/javascript" src="js/cufon-yui.js"></script>
	
	<script type="text/javascript" src="js/MyriadPro.font.js"></script>
	<script type="text/javascript" src="js/ArialBold.font.js"></script>
	
	
	<script type="text/javascript" src="js/jquery-func.js"></script>
	<link rel="shortcut icon" type="image/x-icon" href="css/images/favicon.ico" />
</head>
<body>
<!-- Header -->
<div id="header">
	<div class="shell">
		<!-- Logo -->
		<h1 id="logo"><a href="home.html">Amsel-quality for plastic industry</a></h1>
		<!-- /Logo -->
		
		<!-- Navigation -->
		<div id="navigation">
			<ul>
				<li><a href="home.html">HOME</a></li>
				<li><a href="product.html"class="active">PRODUCT</a></li>
				<li><a href="service.html">SERVICE</a></li>
				<li><a href="process.html">PROCESS</a></li>
				<li><a href="about.html">ABOUT</a></li>
				<li><a href="contact.html">CONTACT</a></li>
			</ul>
		</div>
		<!-- /Navigation -->
	</div>
</div>
<br>
<!-- main -->
<div id="main">
	<!-- shell -->
	<div class="shell">
		<!-- box3-->
		<div class="box3">
			<!-- menu -->
			<div class="menu">
				<ul class="top-level">
					
					<li><a href="screws.html">Screws</a>
						<ul class="sub-level">
							<li><a href="pro-scr-ex.html">Extrusion</a></li>
							<li><a href="pro-scr-in.html">Injection</a></li>
						</ul>
					</li>
					<li><a href="barrels.html">Barrels</a>
						<ul class="sub-level">
							<li><a href="pro-bar-ex.html">Extrusion</a></li>
							<li><a href="pro-bar-in.html">Injection</a></li>
						</ul>
					</li>
					<li><a href="Tip-Noz-End.html">Tip set/Nozzle/End cap</a></li>
					<li><a href="Gear.html">Gear Pump</a></li>
					<li><a href="Screen.html">Screen Changer</a></li>
					<li><a href="Cleaner.html">Set Cleaner</a></li>
					<li>
						<a href="spray.html">Spray</a>
						<ul class="sub-level">
							<li><a href="mold-sp.html">Mold Release Spray</a></li>
							<li><a href="lub-sp.html">Lubricant Spray</a></li>
							<li><a href="anti-sp.html">Anti Rust Spray</a></li>
							<li><a href="cle-sp.html">Cleaning Spray</a></li>
						</ul>
				</ul>
			</div>
			<!-- /menu-->
		</div>
		<!-- /box3 -->
		<!-- box4-->		
		<div class="box4 last-box">
		<br>
		 		<p style="font-size:24px; font:Trebuchet MS;"><strong>Spray</strong></p>
				<h2></h2>
				<div class="entry">
						<div class="news">
						<ul>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/spray/s3 5.jpg" alt="#" /></a>
								</div>
								<div class="post-data2">
									<p><strong>Pelicoat Paintable</strong></p>
									<p>Pelicoat Paintable (S3-5) has secondary manufacturing properties, when painting, printing, glinding or hot stamping is further applied on injection part<a href="#">&hellip;</a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/spray/alpha.jpg" alt="#" /></a>
								</div>
								<div class="post-data2">
									<p><strong>Pelicoat Alpha</strong></p>
									<p>Pelicoat Alpha has outstanding release performance. It is only applicable to injection parts without secondary manufacturing process such as painting, printing, glinding or hot stamping<a href="#">&hellip;</a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/spray/rf.jpg" alt="#" /></a>
								</div>
								<div class="post-data2">
									<p><strong>Pelicoat RF</strong></p>
									<p>Pelicoat RF has good performance for reducing defective ratio of the injection parts and improving accuracy of dimensions<a href="#">&hellip;</a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/spray/dry.jpg" alt="#" /></a>
								</div>
								<div class="post-data2">
									<p><strong>Metal Protect Dry(Quick drying lubricant)</strong></p>
									<p>Fro the push pins & sliding surfaces of molds or precision instruments. Also usable as short term rust protection<a href="#">&hellip;</a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/spray/mo.jpg" alt="#" /></a>
								</div>
								<div class="post-data2">
									<p><strong>Mo Super Grease</strong></p>
									<p>To prevent the metal burn, For low maintance part or part having difficulty to do greasing<a href="#">&hellip;</a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/spray/ef c.jpg" alt="#" /></a>
								</div>
								<div class="post-data2">
									<p><strong>Efflux F/Colored(solid film rust preventive for molds)</strong></p>
									<p>The oilless anti-rust spray uses to protect mold from rust and dust, especially for the mold which dislike oil<a href="#">&hellip;</a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/spray/sabi.jpg" alt="#" /></a>
								</div>
								<div class="post-data2">
									<p><strong>Sabicro(Semi-rigid film rust preventive for molds)</strong></p>
									<p>The oil anti-rust spray has excellent rust preventive performance among oil spray types<a href="#">&hellip;</a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/spray/12.jpg" alt="#" /></a>
								</div>
								<div class="post-data2">
									<p><strong>Pelicoat 12(Multipurpose rust preventive & lubricant)</strong></p>
									<p>Multipurpose spray use for rust preventive and lubrication of machineries and instrument<a href="#">&hellip;</a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/spray/gp c.jpg" alt="#" /></a>
								</div>
								<div class="post-data2">
									<p><strong>GP Cleaner(Multipurpose Cleaner)</strong></p>
									<p>To remove grease and oil from surface of molds, products, macgineries, equipments and so on. It is recommended to use together with "Depo Cleaner" for mold cleaning<a href="#">&hellip;</a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/spray/gp c.jpg" alt="#" /></a>
								</div>
								<div class="post-data2">
									<p><strong>B & P Cleaner C5, Jumbo(Intruments Cleaner)</strong></p>
									<p>B & P Cleaner is designed to completely flush out resin powder or metal powder from hydraulic eqiupments such as brake drum, lining, brake pad, chain, gearbox and bearing<a href="#">&hellip;</a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
							<li>
								<div class="post-image2">
									<a href="#"><img src="css/images/spray/depo.jpg" alt="#" /></a>
								</div>
								<div class="post-data2">
									<p><strong>Depo Cleaner(Deposit Remover)</strong></p>
									<p>Depo cleaner is designed to remove and dissolve the sticky resin stain and deposit, caused by gas in molding,out from molds and products. After sprayed on the mold, leave it for 2-3 minutes until the agent penetrate into the stain then wipe out by clothe or "GP Cleaner"<a href="#">&hellip;</a></p>
								</div>
								<div class="cl">&nbsp;</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="cl">&nbsp;</div>
					
		</div>
		<!-- /box4 -->
		</div>
	<!-- /shell-->		
	<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
</div>
<!-- /main -->
<div id="footer">
	<div class="shell">
		<!-- Mini Nav --> 
		<div class="footer-navigation"> 
			<ul> 
				<li><a href="home.html" >Home</a></li>
				<li><a href="product.html" >Product</a></li>
				<li><a href="service.html">Service</a></li>
				<li><a href="process.html">ProcessS</a></li>
				<li><a href="about.html">About</a></li>
				<li class="last"><a href="contact.html">Contact</a></li> 
			</ul> 
		</div> 
		<div class="right">
		<p> 2/25 Seree 4 Road, Suanluang, Bangkok 10250</p>
		<p>Tel: (66)26646136 </p>
		<p>Fax: (66)26656137</p>
		</div>
		<!-- /Copyrights --> 
		
		<div class="cl">&nbsp;</div> 
	</div>
</div>
</body>
</html>